package org.antwalk;

import java.lang.reflect.Method;

import org.springframework.aop.AfterReturningAdvice;

public class AfterAdvice implements AfterReturningAdvice{

	public void afterReturning(Object returnValue, Method method, Object[] args, Object target) throws Throwable {
		System.out.println("\n---Message from After Advice----");
		Student s = (Student) target;
		
		Double res =s.calculateAvgMarks();
		
		if(res>50)
			System.out.println("Good work keep it up");
		else
			System.out.println("You have to work hard");
	}

}
