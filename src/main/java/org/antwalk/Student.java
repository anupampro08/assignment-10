package org.antwalk;

import java.util.Scanner;

public class Student {
	private String name;
	private double engMarks;
	private double hindiMarks;
	private double mathMarks;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getEngMarks() {
		return engMarks;
	}
	public void setEngMarks(int engMarks) {
		this.engMarks = engMarks;
	}
	public double getHindiMarks() {
		return hindiMarks;
	}
	public void setHindiMarks(int hindiMarks) {
		this.hindiMarks = hindiMarks;
	}
	public double getMathMarks() {
		return mathMarks;
	}
	public void setMathMarks(int mathMarks) {
		this.mathMarks = mathMarks;
	}
	public Student(String name, int engMarks, int hindiMarks, int mathMarks) {
		super();
		this.name = name;
		this.engMarks = engMarks;
		this.hindiMarks = hindiMarks;
		this.mathMarks = mathMarks;
	}
	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public double calculateAvgMarks() {
		return (engMarks + hindiMarks + mathMarks)/3 ;
	}
	
	public void showResult() {
		System.out.println("Result: "+calculateAvgMarks());
	}
	public void setDetails() {
		Scanner in = new Scanner(System.in);
		
		System.out.println("Enter name: ");
		name = in.next();
		
		System.out.println("Enter Math Marks: ");
		mathMarks = in.nextDouble();
		
		System.out.println("Enter Eng Marks: ");
		engMarks = in.nextDouble();
		
		System.out.println("Enter Hindi Marks: ");
		hindiMarks = in.nextDouble();
		
		
	}

}
